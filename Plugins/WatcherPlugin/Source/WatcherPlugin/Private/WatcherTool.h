#pragma once
#include "IDetailCustomization.h"
#include "Input/Reply.h"
#include "WatcherTool.generated.h"

// Helpers
static FReply ExecuteToolCommand(IDetailLayoutBuilder* layout_builder, UFunction* func);
uint32 GetTypeHash(const struct FWatchInfo& info);

USTRUCT()
struct FWatchInfo
{
	GENERATED_BODY()

		FWatchInfo() = default;
	FWatchInfo(UObject* o, UProperty* p, AActor* a) : object(o), prop(p), owner(a) {}

	UObject* object;
	UProperty* prop;
	AActor* owner;

	bool operator==(const FWatchInfo& other) const
	{
		return GetTypeHash(*this) == GetTypeHash(other);
	}
};

class SWatchTextBlock : public STextBlock, public FTickableEditorObject
{
public:
	virtual void Tick(float delta) override;
	virtual TStatId GetStatId() const override { return TStatId(); }

	void SetWatchInfo(const FWatchInfo& info) { m_WatchInfo = info; }

private:
	FWatchInfo m_WatchInfo;
};

////////////////////////////////////////////////////////////////////////////////////////////////
UCLASS(Blueprintable)
class UWatcherTool : public UObject
{
	GENERATED_BODY()

public:
	class FCustomization : public IDetailCustomization
	{
	public:
		static TSharedRef<IDetailCustomization> GetInstance();
		virtual void CustomizeDetails(IDetailLayoutBuilder& layout_builder) override;
	};

	////////////////////////////////////////////////////////////////////////////////////////////
	static TSharedPtr<UWatcherTool> GetInstance();
	static TSharedRef<SWindow> GetWindow();

	UFUNCTION(Exec) void Add();
	UFUNCTION(Exec) void Clear();

	const TSet<FWatchInfo>& GetWatchData();
	TSet<FWatchInfo>& GetWatchDataWrite();

private:
	UPROPERTY() TSet<FWatchInfo> m_WatchData;
};

UCLASS()
class UWatcherPropPicker : public UObject
{
	GENERATED_BODY()

public:
	class FCustomization : public IDetailCustomization
	{
	public:
		static TSharedRef<IDetailCustomization> GetInstance();
		virtual void CustomizeDetails(IDetailLayoutBuilder& layout_builder) override;
	};

	////////////////////////////////////////////////////////////////////////////////////////////
	static TSharedPtr<UWatcherPropPicker> GetInstance();
	static TSharedRef<SWindow> GetWindow();

	void ExportWatch();
	TArray<FWatchInfo>& GetNewWatchDataWrite();

private:
	UPROPERTY() TArray<FWatchInfo> m_NewWatchData; // currently being imported
};

void RefreshLayouts();