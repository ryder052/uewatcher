// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "IWatcherModule.h"
#include "PropertyEditorModule.h"
#include "LevelEditor.h"
#include "WatcherTool.h"
#include "Slate.h"

// Implement module
class FWatcherModule : public IWatcherModule
{
	virtual void StartupModule() override;
};

IMPLEMENT_MODULE( FWatcherModule, WatcherPlugin )

void FWatcherModule::StartupModule()
{
	FPropertyEditorModule& property_editor_module = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	property_editor_module.RegisterCustomClassLayout("WatcherTool", FOnGetDetailCustomizationInstance::CreateStatic(&UWatcherTool::FCustomization::GetInstance));
	property_editor_module.RegisterCustomClassLayout("WatcherPropPicker", FOnGetDetailCustomizationInstance::CreateStatic(&UWatcherPropPicker::FCustomization::GetInstance));

	TSharedRef<FExtender> menu_extender(new FExtender());
	menu_extender->AddMenuExtension(
		"LevelEditor",
		EExtensionHook::After,
		MakeShareable(new FUICommandList()),
		FMenuExtensionDelegate::CreateLambda([](FMenuBuilder& menu_builder) 
		{
			static FText desc = FText::FromString("Actor Watcher");
			static FText tooltip = FText::FromString("Narra Util");
			FUIAction action = FExecuteAction::CreateLambda([]() { UWatcherTool::GetWindow()->ShowWindow(); });

			menu_builder.AddMenuEntry(desc, tooltip, FSlateIcon(), action);
		})
	);

	FLevelEditorModule& level_editor_module = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
	level_editor_module.GetMenuExtensibilityManager()->AddExtender(menu_extender);
}



