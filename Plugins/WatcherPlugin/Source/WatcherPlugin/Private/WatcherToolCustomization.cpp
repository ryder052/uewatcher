#include "CoreMinimal.h"
#include "WatcherTool.h"
#include "DetailLayoutBuilder.h"
#include "DetailCategoryBuilder.h"
#include "DetailWidgetRow.h"
#include "Slate.h"
#include "Engine.h"
#include "Editor.h"

FReply ExecuteToolCommand(IDetailLayoutBuilder* layout_builder, UFunction* func)
{
	TArray<TWeakObjectPtr<UObject>> customized_objects;
	layout_builder->GetObjectsBeingCustomized(customized_objects);

	for (auto obj : customized_objects)
	{
		if (auto* instance = obj.Get())
		{
			instance->CallFunctionByNameWithArguments(*func->GetName(), *GLog, nullptr, true);
		}
	}

	return FReply::Handled();
}

FString GetWatchName(UProperty* prop, UObject* object, AActor* actor = nullptr)
{
	FString prefix = actor ? TEXT("[") + actor->GetName() + TEXT("]") : TEXT("");

	return prefix + TEXT("[") + object->GetName() + TEXT("] ") + prop->GetName();
}

void SWatchTextBlock::Tick(float)
{
	if (Visibility.Get() == EVisibility::Hidden || Visibility.Get() == EVisibility::Collapsed)
		return;

	FString value_str;
	const uint8* addr = m_WatchInfo.prop->ContainerPtrToValuePtr<uint8>(m_WatchInfo.object);
	m_WatchInfo.prop->ExportTextItem(value_str, addr, nullptr, nullptr, PPF_None);

	SetText(FText::FromString(value_str));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TSharedRef<IDetailCustomization> UWatcherTool::FCustomization::GetInstance()
{
	return MakeShareable(new UWatcherTool::FCustomization);
}

void UWatcherTool::FCustomization::CustomizeDetails(IDetailLayoutBuilder& layout_builder)
{
	// Actions
	{
		IDetailCategoryBuilder& category = layout_builder.EditCategory("Actions");

		for (TFieldIterator<UFunction> it(UWatcherTool::StaticClass()); it; ++it)
		{
			UFunction* func = *it;
			if (!(func->HasAnyFunctionFlags(FUNC_Exec) && func->NumParms == 0))
				continue;

			const FText text = FText::FromString(func->GetName());
			FDetailWidgetRow& new_row = category.AddCustomRow(text);

			new_row.NameContent()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					[
						SNew(SButton)
						.Text(text)
						.OnClicked(FOnClicked::CreateStatic(&ExecuteToolCommand, &layout_builder, func))
					]
				];
		}
	}

	// Watch
	{
		IDetailCategoryBuilder& category = layout_builder.EditCategory("Watch");
		auto& data = UWatcherTool::GetInstance()->GetWatchDataWrite();
		for (auto&& watch_info : data)
		{
			FText text = FText::FromString(GetWatchName(watch_info.prop, watch_info.object, watch_info.owner));
			FDetailWidgetRow& new_row = category.AddCustomRow(text);
			new_row.NameContent()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					.Padding(FMargin{ 0, 0, 20, 0 })
					[
						SNew(SButton)
						.Text(FText::FromString(TEXT("X")))
						.OnClicked_Lambda([watch_info, &data]
						{ 
							data.Remove(watch_info);
							RefreshLayouts();
							return FReply::Handled();	
						})
					]
					+ SHorizontalBox::Slot()
					.AutoWidth()
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(STextBlock).Text(text)
						.Font(IDetailLayoutBuilder::GetDetailFont())
					]
				];

			TSharedPtr<SWatchTextBlock> text_block;
			SAssignNew(text_block, SWatchTextBlock);
			text_block->SetWatchInfo(watch_info);
			text_block->SetFont(IDetailLayoutBuilder::GetDetailFont());

			new_row.ValueContent()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					.Padding(FMargin{ 10, 0, 0, 0 })
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						text_block.ToSharedRef()
					]
				];
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
TSharedRef<IDetailCustomization> UWatcherPropPicker::FCustomization::GetInstance()
{
	return MakeShareable(new UWatcherPropPicker::FCustomization);
}

void AddObjectWatch(UObject* object, IDetailCategoryBuilder& category, TArray<FWatchInfo>& data, AActor* actor = nullptr)
{
	for (TFieldIterator<UProperty> prop_it(object->GetClass()); prop_it; ++prop_it)
	{
		UProperty* prop = *prop_it;
		const FText name_text = FText::FromString(GetWatchName(prop, object, actor));

		FDetailWidgetRow& new_row = category.AddCustomRow(name_text);
		new_row.NameContent()
			.MaxDesiredWidth(30.0f)
			[
				SNew(SCheckBox)
				.OnCheckStateChanged_Lambda([&data, object, actor, prop](ECheckBoxState s)
				{
					if (s == ECheckBoxState::Checked)
						data.Add({ object, prop, actor });
					else
						data.Remove({ object, prop, actor });
				})
			];

		new_row.ValueContent()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(STextBlock).Text(name_text).Font(IDetailLayoutBuilder::GetDetailFont())
				]
			];
	}
}

void UWatcherPropPicker::FCustomization::CustomizeDetails(IDetailLayoutBuilder& layout_builder)
{
	// Properties
	{
		IDetailCategoryBuilder& category = layout_builder.EditCategory("Properties");
		auto& data = UWatcherPropPicker::GetInstance()->GetNewWatchDataWrite();

		USelection* selected_actors = GEditor->GetSelectedActors();

		for (FSelectionIterator actor_it(*selected_actors); actor_it; ++actor_it)
		{
			AddObjectWatch(*actor_it, category, data);

			// Process components
			auto* actor = Cast<AActor>(*actor_it);
			for (auto&& component : actor->GetComponents())
				AddObjectWatch(component, category, data, actor);
		}
	}
}