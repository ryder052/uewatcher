#include "CoreMinimal.h"
#include "WatcherTool.h"
#include "Engine.h"
#include "Editor.h"

void RefreshLayouts()
{
	FPropertyEditorModule& property_editor_module = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
	property_editor_module.NotifyCustomizationModuleChanged();
}

uint32 GetTypeHash(const FWatchInfo& info)
{
	FString prefix = info.owner ? info.owner->GetName() : TEXT("");
	return GetTypeHash(prefix + info.object->GetName() + info.prop->GetNameCPP());
}

TSharedPtr<UWatcherTool> UWatcherTool::GetInstance()
{
	static TSharedPtr<UWatcherTool> instance(NewObject<UWatcherTool>());
	static bool inited = false;
	if (!inited)
	{
		instance->AddToRoot();
		inited = true;
	}

	return instance.IsValid() ? instance : nullptr;
}

TSharedRef<SWindow> UWatcherTool::GetWindow()
{
	FPropertyEditorModule& property_editor_module = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

	static TSharedRef<SWindow> window = property_editor_module.CreateFloatingDetailsView({ GetInstance().Get() }, true);
	static bool inited = false;
	if (!inited)
	{
		window->SetTitle(FText::FromString(TEXT("Actor Watch")));
		window->Resize({ 900, 500 });
		window->SetRequestDestroyWindowOverride(FRequestDestroyWindowOverride::CreateLambda([](const TSharedRef<SWindow>& w) { w->HideWindow(); }));
		inited = true;
	}

	return window;
}

void UWatcherTool::Add()
{
	UWatcherPropPicker::GetWindow()->ShowWindow();
	RefreshLayouts();
}

void UWatcherTool::Clear()
{
	m_WatchData.Empty();
	RefreshLayouts();
}

const TSet<FWatchInfo>& UWatcherTool::GetWatchData()
{
	return m_WatchData;
}

TSet<FWatchInfo>& UWatcherTool::GetWatchDataWrite()
{
	return m_WatchData;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TSharedPtr<UWatcherPropPicker> UWatcherPropPicker::GetInstance()
{
	static TSharedPtr<UWatcherPropPicker> instance(NewObject<UWatcherPropPicker>());
	static bool inited = false;
	if (!inited)
	{
		instance->AddToRoot();
		inited = true;
	}

	return instance.IsValid() ? instance : nullptr;
}

TSharedRef<SWindow> UWatcherPropPicker::GetWindow()
{
	FPropertyEditorModule& property_editor_module = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

	static TSharedRef<SWindow> window = property_editor_module.CreateFloatingDetailsView({ GetInstance().Get() }, true);
	static bool inited = false;
	if (!inited)
	{
		window->SetTitle(FText::FromString(TEXT("Property Picker")));
		window->Resize({ 600, 800 });
		window->SetRequestDestroyWindowOverride(FRequestDestroyWindowOverride::CreateLambda([](const TSharedRef<SWindow>& w) 
		{ 
			GetInstance()->ExportWatch();
			w->HideWindow(); 
		}));
		inited = true;
	}

	return window;
}

TArray<FWatchInfo>& UWatcherPropPicker::GetNewWatchDataWrite()
{ 
	return m_NewWatchData;
}

void UWatcherPropPicker::ExportWatch()
{
	auto tool = UWatcherTool::GetInstance();
	if (!tool)
		return;

	tool->GetWatchDataWrite().Append(m_NewWatchData);
	m_NewWatchData.Empty();

	GetWindow()->HideWindow();
	RefreshLayouts();
}
