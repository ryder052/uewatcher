// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Watcher.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Watcher, "Watcher" );
 